<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## O projekcie

Aplikacja Fullstack stworzona, aby publikować swoje ogłoszenia samochodowe.

## Instalacja

-   composer install

-   npm install

-   Skopiuj plik „.env.example” i zmień nazwę na „.env”

-   php artisan serve

-   npm run dev

-   php artisan key:generate

-   php artisan migrate

-   php artisan db:seed

-   php artisan storage:link

## Wygląd aplikacji

Login: admin@gmail.com
Haslo: haslo1234

## Wygląd aplikacji

